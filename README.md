# stylelint

[stylelint](https://github.com/hadolint/hadolint) is a CSS linter that helps
you avoid errors and enforce conventions.

## Using in the Pipeline

To enable stylelint in your project's pipeline add `stylelint` to the
`ENABLE_JOBS` variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "stylelint"
```

The ENABLE_JOBS variable is a comma-separated string value, eg.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install [stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint)
for Visual Studio Code.

## Using Locally in Your Development Environment

stylelint can be run locally in a linux-based bash shell or in a linting shell
script with this Docker command:

```bash
docker run -it -v "${PWD}:/workdir" ghcr.io/cardboardci/stylelint:edge \
    "stylelint **/*.{css,vue,js}"
```

## Configuration

Each project should have a `.stylelintrc.json` file which is used for
[stylelint configuration](https://stylelint.io/user-guide/configure).

This file is used by the pipeline tool, the Visual Studio Code extension,
and the locally run Docker image.

This project has a `.stylelintrc.json` file that uses
[stylelint-config-standard](https://github.com/stylelint/stylelint-config-standard/blob/HEAD/index.js)
that can be used as an example.

## Licensing

- Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
- Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
- Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
